# Getting Started

This tutorial will help you to get familiar and start using the following
packages:

* [`bilby`](https://git.ligo.org/lscsoft/bilby)/
[`bilby_pipe`](https://git.ligo.org/lscsoft/bilby_pipe)
* [`PESummary`](https://git.ligo.org/lscsoft/pesummary)

To get things started smoothly we are going to work on the Caltech Institute of 
Technology (CIT) cluster,<br/> which has all the necessary packages and dependencies
already installed for you.

## Open CIT environment

To start, open a new terminal window.<br/>
The CIT cluster is accessible to ALL ligo members, but you will have to input
your credentials to enter.<br/> Type the following command and then enter your
ligo password:
```bash
ligo-proxy-init albert.einsten
```
To access the CIT cluster you need to enter the following command:
```bash
gsissh ldas-pcdev3.ligo.caltech.edu
```
If you have correclty accessed the system, a welcome message should appear.<br/>
To activate the environment containing all the necessary lsc software packages,<br/> 
run the following command (note your prompt will change to indicate you are in
a virtual environment):
```console
[albert.einstein@ldas-pcdev5 ~]$ . /cvmfs/ligo-containers.opensciencegrid.org/lscsoft/conda/latest/bin/activate ligo-py37
(ligo-py37) [albert.einstein@ldas-pcdev5 ~]$
```
You can also access CIT's [jupyter lab](https://jupyter.ligo.caltech.edu/) at
this link. Again, you will be required to provide your albert.einsten
username and ligo password.

## Check required packages work

A quick way to check you have what you need is to start an `ipython` shell and
import them. Run the following code within the ligo virtual environment:
```console
(ligo-py37) [albert.einstein@ldas-pcdev5 ~]$ipython
```
Then import `bilby`, `bilby_pipe` and `pesummary` for a simple check:
```python
In[1]: import bilby
In[2]: import bilby_pipe
In[3]: import pesummary
```
`bilby` and `bilby_pipe` will output a message containing their version number.
